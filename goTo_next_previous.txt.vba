'purpose: To provide a shortcut to document navigation
'         similar to using the GoTo dialog
'version: 1.0
'Usage: save module to 'Normal'. Assign keyboard shortcuts in Normal.dotm
'       suggested key bindings:
'           nextLine: Ctrl Down
'           prevLine: Ctrl Up
'           nextPage: Ctrl PgDn
'           prevLine: Ctrl PgUp
'           nextLineJump: Alt Down
'           prevLineJump: Alt Up

Sub nextLine()
    ' scroll forwards 1 line
    scrollLines (1)
End Sub
Sub prevLine()
    ' scroll back 1 line
    scrollLines (-1)
End Sub
Sub nextLineJump()
    ' move cursor forwards lines
    a = scrollLines(10, False)
End Sub
Sub prevLineJump()
    ' move cursor back lines
    a = scrollLines(-10, False)
End Sub
Sub nextPage()
' Default behaviour for this can be found via Ctrl PgDn
' However, if nextLine or prevLine is used, this behaviour
' is overwritten, hence the need for dedicated macros
    Selection.GoTo What:=wdGoToPage, Which:=wdGoToNext, Count:=1
End Sub
Sub prevPage()
    Selection.GoTo What:=wdGoToPage, Which:=wdGoToPrevious, Count:=1
End Sub

Function scrollLines(iNum As Integer, Optional bScroll As Boolean = True)
    'purpose: scroll the page by a specified number of lines
    'in: iNum: number of lines by which to scroll.
            ' if negative, scroll up
            ' if positive, scroll down
        'bScroll: if true, a small screen scroll will take place by iNum lines

    'go forwards or back?
    If iNum < 0 Then
        ' negative: go back
        'num just specifies magnitude
        iNum = iNum * -1
        ' move the cursor
        Selection.GoTo What:=wdGoToLine, Which:=wdGoToPrevious, Count:=iNum 'see comments below
        ' scroll the screen
        If bScroll = True Then ActiveWindow.SmallScroll Up:=iNum
    ElseIf iNum > 0 Then
        'postiive: go forwards
        ' move the cursor
        Selection.GoTo What:=wdGoToLine, Which:=wdGoToNext, Count:=iNum
        ' scroll the screen
        If bScroll = True Then ActiveWindow.SmallScroll Down:=iNum
    Else
        Err.Raise vbObjectError + 513
    End If

'Selection: current selection
'What: kind of object to navigate over (line, paragraph, section etc)
'Which: direction to go in the document
'Count: number of objects to navigate over

End Function
